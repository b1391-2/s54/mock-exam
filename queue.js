let collection = [];

// Write the queue functions below.

function print() {
	return collection
}

function enqueue (input) {
	collection[collection.length] = input;
	return collection
}

function dequeue () {
	collection.splice(0,1);
	return collection
}

function front() {
	let firstElement = collection[0];
	return firstElement
}

function size () {
	let collectionSize = collection.length;
	return collectionSize
}

function isEmpty () {
	if (collection.length == 0) {
		return true	
	} else {
		return false
	}
}


module.exports = {
	print,
	enqueue,
	dequeue,
	size,
	front,
	isEmpty

};